# Multifetch Core

Python scripts for multifetch project

## Getting started

These scripts can be used to download the metadata from various sources for the SarsCOV19 database, marine databases etc.

They are also serve as a base for the Multifetch nextflow pipelines (multfetch-mardb-nextflow and multifetch-sarscovid19-nextflow).

Example of usage can be found in the .gitlab-ci file.

Even though these scripts can be used in the same way as in any other python application it is advised to create a separate 
nextflow pipeline similar to multfetch-mardb-nextflow and multifetch-sarscovid19-nextflow projects.

For example, if you want to modify miltifetch project to download data from the different source or use it in different project,
it can be done with the following steps:
 - Create a separate branch from the multifetch-core, make necesary modifications, test it with the short list of ids.
 - Modify gitlab jobs in .gitlab-ci file to ensure the script is running.
 - Make a release based on this branch
 - Create new project or branch from either multfetch-mardb-nextflow or multifetch-sarscovid19-nextflow
 - Use released version of the multifetch-core and write a nextflow pipeline
 - Run and test
 - Use this pipeline in production either manually or as a part of the gitlab pipeline



