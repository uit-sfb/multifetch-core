from multiprocessing import Pool, cpu_count
from functools import partial
from utils import *
import datetime
import os
from pathlib import Path
import sys
import argparse
import shutil
import urllib3
from http.client import responses


def main(argv):
    parser = argparse.ArgumentParser(description='Set email and other parameters')
    parser.add_argument('-i', dest='inputList', default='', help='list of accessions')
    # parser.add_argument('-l', dest='lost', default='', help='output for lost records list')
    parser.add_argument('-o', dest='out', default='', help='output directory')
    parser.add_argument('--one_proc', dest='one_proc', action='store_true', default=False,
                        help='run in only one process')
    args = parser.parse_args()
    return args.inputList, args.out, args.one_proc


# def create_subdir(path):
#    Path(path).mkdir(parents=True, exist_ok=True)
#    return path


# def move_files(source, destination):
#    file_names = os.listdir(source)
#    for file_name in file_names:
#        if os.path.exists(source):
#            shutil.move(os.path.join(source, file_name), os.path.join(destination, file_name))


# def remove_dir(tmp):
#    dirpath = Path(tmp)
#    if dirpath.exists() and dirpath.is_dir():
#        os.rmdir(dirpath)


# def remove_folder(folder):
#    dirpath = Path(folder)
#    if dirpath.exists() and dirpath.is_dir():
#        shutil.rmtree(dirpath, ignore_errors=True)


# def clean_empty(path):
#    try:
#        os.remove(filePath)
#    except Exception as e:
#        print("Error while deleting file ", path)
#        print(e)


def downloadEmbl(id, dir):
    Path(dir).mkdir(parents=True, exist_ok=True)
    baseUrl = "https://www.ebi.ac.uk/ena/browser/api/embl"
    lineLimit = "lineLimit=1000000"
    url = baseUrl + '/' + str(id) + '?' + lineLimit
    file_path = os.path.join(dir, '{}.embl'.format(id))
    http = urllib3.PoolManager()
    with http.request('GET', url, preload_content=False) as req:
        http_status = req.status
        http_desc = responses[http_status]
        if not (http_desc == "OK"):
            raise Exception(http_desc)
        else:
            with open(file_path, 'wb') as f:
                shutil.copyfileobj(req, f)
            return file_path


def run(input, proc_flag, tmp):
    records = getRecords(input, "genbank_accession")
    download = partial(downloadEmbl, dir=tmp)
    if not proc_flag:
        pool = Pool(cpu_count() - 1)
        results = pool.map(download, records)
        pool.close()
        pool.join()
    else:
        print("Downloading embl files in one process")
        results = list(map(download, records))
    return results


if __name__ == "__main__":
    input, out, proc_flag = main(sys.argv[1:])
    output = create_subdir(out)
    tmpdir = "/tmp/embl_tmp"  # tmp
    remove_folder(tmpdir)
    tmp = create_subdir(tmpdir)
    run(input, proc_flag, tmp)
    move_files(tmp, output)
    remove_folder(tmpdir)
