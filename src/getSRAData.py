from utils import *
import xml.etree.ElementTree as ET
from http.client import responses
import urllib3


def getSeqMethods(data):
    root = ET.fromstring(data)
    attrs = root.findall('./SAMPLE/SAMPLE_ATTRIBUTES/SAMPLE_ATTRIBUTE/TAG')
    attr_vals = root.findall('./SAMPLE/SAMPLE_ATTRIBUTES/SAMPLE_ATTRIBUTE/VALUE')
    seq_methods_index = [attrs.index(s) for s in attrs if 'seq_method' in s.text]
    if seq_methods_index:
        return attr_vals[seq_methods_index[0]].text
    return ''


def processSRAData(accession, id):
    baseUrl = "https://www.ebi.ac.uk/ena/browser/api/xml"
    url = baseUrl + '/' + str(id)
    http = urllib3.PoolManager()
    request = http.request('GET', url)
    http_status = request.status
    http_desc = responses[http_status]
    df = pd.DataFrame()
    if not (http_desc == "OK"):
        pass
        # print("SRA {} not fetched, reason: ".format(id) + str(http_status) + " " + str(http_desc))
    else:
        data = request.data.decode('UTF-8')
        df = pd.DataFrame(dict([('acc:genbank', accession), ('sra:seq:method', getSeqMethods(data))]), index=[0])
    if not df.empty:
        df.columns = list(map(formatColumn, list(df.columns.values)))
    return df
