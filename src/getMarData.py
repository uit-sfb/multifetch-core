from getBioSampleData import *
from getBioProjectData import *
from multiprocessing import Pool, cpu_count
from utils import *
from time import time as timer
from functools import partial
import sys
import argparse
import datetime
import urllib3
from http.client import responses
import pandas as pd

def main(argv):
    parser = argparse.ArgumentParser(description='Set email and other parameters')
    parser.add_argument('-i', dest='input', default='', help='list of accessions')
    parser.add_argument('-o', dest='out', default='', help='output directory')
    parser.add_argument('-t', dest='table', default='', help='path to table of attributes')
    args = parser.parse_args()
    return args.input, args.out, args.table;

# def getPatricData(input):
#     if input:
#         patric_df = getTSVInput(input)
#         patric_df.rename(columns={'assembly_accession':'acc:assembly'}, inplace=True)  # TODO: rewrite hardcoding of column name
#         cols = list(patric_df)
#         # move the column to head of list using index, pop and insert
#         cols.insert(0, cols.pop(cols.index('acc:assembly')))
#         patric_df = patric_df.loc[:, cols]
#         return patric_df
#     return pd.DataFrame()

#
# def getRowFromPatric(id, df):
#     if not df.empty:
#         res = df.loc[df['acc:assembly'] == str(id)]
#         return res
#     return df

def processXMLData(root,primary_id,attr_defs):
    attrs = root.findall('./ASSEMBLY/ASSEMBLY_ATTRIBUTES/ASSEMBLY_ATTRIBUTE/TAG')
    attr_vals = root.findall('./ASSEMBLY/ASSEMBLY_ATTRIBUTES/ASSEMBLY_ATTRIBUTE/VALUE')
    elems = dict([elem for elem in zip(attrs, attr_vals)])
    tagsAndValues = [("ena:" + str(t.text), v.text) for t, v in elems.items()]
    # primary_id = root.find('./ASSEMBLY/IDENTIFIERS/PRIMARY_ID')
    taxon_id, scientific_name, strain, *_ = list(map(lambda x: x.text if x != None else None,[root.find('./ASSEMBLY/TAXON/TAXON_ID'), root.find('./ASSEMBLY/TAXON/SCIENTIFIC_NAME'), root.find('./ASSEMBLY/TAXON/STRAIN')]))
    strain_taxon_name = [("ena:tax:taxon_id", taxon_id), ("ena:host:scientific_name", scientific_name), ("ena:tax:strain", strain)]
    assembly_df = pd.DataFrame(dict([("ena:acc:assembly", primary_id.text)] + tagsAndValues + strain_taxon_name), index=[0])

    assembly_df = updateAttributes(assembly_df, attr_defs) #attr_defs.get('ENA')
    return assembly_df

def getXMLData(id):
    baseUrl = "https://www.ebi.ac.uk/ena/browser/api/xml"
    url = baseUrl + '/' + str(id)
    http = urllib3.PoolManager()
    with http.request('GET',url, preload_content=False) as req:
        http_status = req.status
        http_desc = responses[http_status]
        if not (http_desc == "OK"):
            raise Exception(http_desc)
        else:
            return req.data.decode('UTF-8')

# def getXMLAttributes(data, attr_defs): #patric_df
#     root = ET.fromstring(data)
#     df = pd.DataFrame()
#     attrs = root.findall('./ASSEMBLY/ASSEMBLY_ATTRIBUTES/ASSEMBLY_ATTRIBUTE/TAG')
#     attr_vals = root.findall('./ASSEMBLY/ASSEMBLY_ATTRIBUTES/ASSEMBLY_ATTRIBUTE/VALUE')
#     elems = dict([elem for elem in zip(attrs, attr_vals)])
#     tagsAndValues = [("ena:" + str(t.text), v.text) for t, v in elems.items()]
#
#     primary_id = root.find('./ASSEMBLY/IDENTIFIERS/PRIMARY_ID')
#     # patric_row_df = getRowFromPatric(primary_id.text, patric_df)
#     taxon_id, scientific_name, strain, *_ = list(map(lambda x: x.text if x != None else None,[root.find('./ASSEMBLY/TAXON/TAXON_ID'), root.find('./ASSEMBLY/TAXON/SCIENTIFIC_NAME'), root.find('./ASSEMBLY/TAXON/STRAIN')]))
#
#     strain_taxon_name = [("ena:tax:taxon_id", taxon_id), ("ena:host:scientific_name", scientific_name), ("ena:tax:strain", strain)]
#
#     assembly_df = pd.DataFrame(dict([("ena:acc:assembly", primary_id.text)] + tagsAndValues + strain_taxon_name), index=[0])
#     assembly_df = updateAttributes(assembly_df, attr_defs.get('ENA'))
#
#     biosample_ref = root.find('./ASSEMBLY/SAMPLE_REF/IDENTIFIERS/PRIMARY_ID')
#     bioproject_ref = root.find('./ASSEMBLY/STUDY_REF/IDENTIFIERS/PRIMARY_ID')
#     bio_df = pd.DataFrame()
#     proj_df = pd.DataFrame()
#
#     if biosample_ref != None:
#         bio_df = processBioSampleData(primary_id.text, biosample_ref.text)
#         bio_df = updateAttributes(bio_df, attr_defs.get('BioSample'))
#         bio_df.rename(columns={'acc:genbank':'acc:assembly'}, inplace=True) # must be FIRST column in the left merged dataframe
#
#     if bioproject_ref != None:
#         proj_df = processBioProjectData(primary_id.text, bioproject_ref.text)
#         proj_df = updateAttributes(proj_df, attr_defs.get('BioProject'))
#         proj_df.rename(columns={'acc:genbank':'acc:assembly'}, inplace=True)
#
#     df = mergeDataFrames(proj_df, bio_df)
#     df = mergeDataFrames(df, assembly_df)
#     # df = mergeDataFrames(df, patric_row_df)
#     df = df.groupby(level=0, axis=1).apply(lambda x: x.apply(sjoin, axis=1))
#     df = removeDuplicatedData(df)
#     return df

# def processXMLData(id, attr_defs): #patric_df
#     baseUrl = "https://www.ebi.ac.uk/ena/browser/api/xml"
#     url = baseUrl + '/' + str(id)
#     http = urllib3.PoolManager()
#     request = http.request('GET', url)
#     http_status = request.status
#     http_desc = responses[http_status]
#     df = pd.DataFrame()
#     if not (http_desc == "OK"):
#         print("XML {} not fetched, reason: ".format(id) + str(http_status) + " " + str(http_desc))
#         return (df, id)
#     else:
#         try:
#             data = request.data.decode('UTF-8')
#             #parse xml assembly, project and sample data
#             df = getXMLAttributes(data, attr_defs) #patric_df
#         except Exception as e:
#             print("Failed to process assembly, bioproject and biosample data, \n{}".format(e))
#     if not df.empty:
#         df.columns = list(map(formatColumn, list(df.columns.values)))
#     return df, ''

#
# def split_data(lst):
#     lost, found = [], []
#     for t in lst:
#         if t[1]:
#             lost.append(t[1])
#         else:
#             found.append(t[0])
#     return lost, found
def createDataframes(id,attr_defs):
    xml_data = getXMLData(id)
    root = ET.fromstring(xml_data)
    primary_id = root.find('./ASSEMBLY/IDENTIFIERS/PRIMARY_ID')
    assembly_df = processXMLData(root,primary_id,attr_defs.get('ENA'))

    biosample_ref = root.find('./ASSEMBLY/SAMPLE_REF/IDENTIFIERS/PRIMARY_ID')
    bioproject_ref = root.find('./ASSEMBLY/STUDY_REF/IDENTIFIERS/PRIMARY_ID')
    bio_df = pd.DataFrame()
    proj_df = pd.DataFrame()

    if biosample_ref != None:
        bio_df = processBioSampleData(primary_id.text, biosample_ref.text)
        bio_df = updateAttributes(bio_df, attr_defs.get('BioSample'))
        bio_df.rename(columns={'acc:genbank':'acc:assembly'}, inplace=True) # must be FIRST column in the left merged dataframe

    if bioproject_ref != None:
        proj_df = processBioProjectData(primary_id.text, bioproject_ref.text)
        proj_df = updateAttributes(proj_df, attr_defs.get('BioProject'))
        proj_df.rename(columns={'acc:genbank':'acc:assembly'}, inplace=True)


    df = mergeDataFrames(proj_df, bio_df)
    df = mergeDataFrames(df, assembly_df)
    df = df.groupby(level=0, axis=1).apply(lambda x: x.apply(sjoin, axis=1))
    df = removeDuplicatedData(df)
    df = df.applymap(removeTabs)

    return df


def run(input, output, table):

    records = getRecords(input,'acc:assembly_genbank')
    attrs = getAllAttributes(table)
    attr_defs = getAttributeOrder(table)

    default_values = zip(attrs, getDefaultValues(table))
    default_df = pd.DataFrame(dict(default_values), index=[0])
    default_df.dropna(how='all', axis=1, inplace=True)


    df = pd.DataFrame(columns=attrs)

    pool = Pool(cpu_count()-1)

    p_func = partial(createDataframes, attr_defs = attr_defs) #patric_df = patric_df
    df_list = pool.map(p_func, records)
    pool.close()
    pool.join()
#     print("Concatenate results into DataFrame")
#     lost_list, df_list = split_data(data_list)
    df = df.append(df_list)
    df = addPrefix(df, attrs)
    df.reset_index(drop=True, inplace=True)
    df = capitalizeValues(df)
    writeToTSV(df, output)


if __name__ == "__main__":
    input, output, table = main(sys.argv[1:])
    run(input, output, table)

