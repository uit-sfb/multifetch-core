from multiprocessing import Pool, cpu_count
from functools import partial
from Bio import Entrez
import sys
import xml.etree.ElementTree as ET
import gzip
from time import sleep
from urllib import parse
from utils import *
import ftplib
import errno
import uuid
import zlib


class RecordIdsObj:
    def __init__(self, assembly, idList, version):
        self.assembly = assembly
        self.version = version
        self.idList = idList


def main(argv):
    parser = argparse.ArgumentParser(description='Set email and other parameters')
    parser.add_argument('-i', dest='input', default='', help='list of accessions')
    parser.add_argument('-o', dest='out', default='', help='output directory')
    parser.add_argument('--email', dest='email', default='', help='entrez email')
    parser.add_argument('--api-key', dest='key', default='', help='entrez api-key')
    args = parser.parse_args()
    return args.input, args.out, args.email, args.key


def get_path(path_obj):
    if path_obj is not None:
        return path_obj.text
    return None


def get_ftp_paths(url_path):
    postfix = url_path.split("/")[-1]
    ftp_path_fasta = os.path.join(url_path, "{}_genomic.fna.gz".format(postfix))
    ftp_path_nuc = os.path.join(url_path, "{}_cds_from_genomic.fna.gz".format(postfix))
    ftp_path_prot = os.path.join(url_path, "{}_protein.faa.gz".format(postfix))
    return ftp_path_fasta, ftp_path_nuc, ftp_path_prot


def get_url_paths(path_list):
    urls = flattenList(list(map(get_ftp_paths, path_list)))
    return urls


def get_recent_version(records):
    res = list(map(get_record_obj, records))
    return res


def get_record_obj(r):
    handle_rec = Entrez.esearch(db="assembly", term=r)
    record = Entrez.read(handle_rec)
    handle_rec.close()
    obj = RecordIdsObj(assembly=f"{r}",version=f"{record.get('Count')}",idList=record.get("IdList"))
    sleep(0.1)
    return obj


def generate_urls(record_obj, out_dir):
    file_path = os.path.join(out_dir, record_obj.assembly)
    if not os.path.exists(file_path) or (os.path.isdir(file_path) and not os.listdir(file_path)):
        for acc_id in record_obj.idList:
            handle = Entrez.esummary(db="assembly", id=acc_id, report="full")
            tree = ET.fromstring(handle.read())
            handle.close()
            gca_path = get_path(tree.find('./DocumentSummarySet/DocumentSummary/FtpPath_GenBank'))
            path_list = list(filter(lambda x: x is not None, [gca_path]))
            urls = get_url_paths(path_list)
            return urls


def rename_fasta(filename):
    if "cds_from_genomic.fna" in filename:
        return "cds.fna"
    if "genomic.fna" in filename:
        return "assembly.fa"
    if "protein.faa" in filename:
        return "protein.faa"
    print("{} is undefined".format(filename))
    return "undefined"


def get_id_name(url):
    elems = os.path.split(parse.urlsplit(url).path)[-1].split("_")[:2]
    id = "_".join(elems)
    return id


# TODO instead of saving files to memory use smart object structure?
def download_data_ftp(url, tmp):
    elems = url.split("/")
    ftp_directory = "/" + "/".join(elems[3:-1])
    ftp_host = elems[:3].pop()
    filename = elems.pop()
    tmp_filename = tmp + ".gz"
    completed = False
    with ftplib.FTP(ftp_host) as ftp:
        ftp.login()
        ftp.cwd(ftp_directory)
        for i in range(10):
            if completed:
                break
            try:
                with open(tmp_filename, "wb") as f_handle:
                    ftp.retrbinary("RETR " + filename, f_handle.write)
                with gzip.open(tmp_filename, mode='rb') as gz_in:
                    with open(tmp, mode='wb') as f_out:
                        shutil.copyfileobj(gz_in, f_out)
                        completed = True
                        if os.path.exists(tmp_filename):
                            os.unlink(tmp_filename)
            except zlib.error as err:
                if os.path.exists(tmp_filename):
                    os.unlink(tmp_filename)
                print("Entry:{} ".format(filename), err)
    if os.path.exists(tmp_filename):
        os.unlink(tmp_filename)


def fetch_data(urls, tmp):
    if urls:
        for url in urls:
            try:
                id = get_id_name(url)
                id = id.split('.')[0]
                tmp_subdir = create_subdir(os.path.join(tmp, id))
                file_name = rename_fasta(os.path.split(parse.urlsplit(url).path)[-1].rstrip('.gz'))
                tmp_path = os.path.join(tmp_subdir, file_name)
                download_data_ftp(url, tmp_path)
            # TODO handle errors separately
            except ftplib.all_errors as err:
                print(err)


def move_atomic(source, destination):
    if os.path.isfile(source):
        safe_move(source, destination)
    else:
        file_names = os.listdir(source)
        for file_name in file_names:
            sourcePath = os.path.join(source, file_name)
            destPath = os.path.join(destination, file_name)
            move_atomic(sourcePath, destPath)


# Source: https://alexwlchan.net/
def safe_move(src, dst):
    """Rename a file from ``src`` to ``dst``.

    *   Moves must be atomic.  ``shutil.move()`` is not atomic.
        Note that multiple threads may try to write to the cache at once,
        so atomicity is required to ensure the serving on one thread doesn't
        pick up a partially saved image from another thread.

    *   Moves must work across filesystems.  Often temp directories and the
        cache directories live on different filesystems.  ``os.rename()`` can
        throw errors if run across filesystems.

    So we try ``os.rename()``, but if we detect a cross-filesystem copy, we
    switch to ``shutil.move()`` with some wrappers to make it atomic.
    """
    try:
        os.rename(src, dst)
    except OSError as err:
        if err.errno == errno.EXDEV:
            # Generate a unique ID, and copy `<src>` to the target directory
            # with a temporary name `<dst>.<ID>.tmp`.  Because we're copying
            # across a filesystem boundary, this initial copy may not be
            # atomic.  We intersperse a random UUID so if different processes
            # are copying into `<dst>`, they don't overlap in their tmp copies.
            copy_id = uuid.uuid4()
            tmp_dst = "%s.%s.tmp" % (dst, copy_id)
            shutil.copyfile(src, tmp_dst)

            # Then do an atomic rename onto the new name, and clean up the
            # source image.
            shutil.move(tmp_dst, dst)
            os.unlink(src)
        else:
            raise


def create_destination_folders(records, destination):
    for record in records:
        create_subdir(os.path.join(destination, record.assembly))


def run(input, output, tmp):
    records = get_recent_version(getRecords(input, 'acc:assembly_genbank'))
    create_destination_folders(records, output)
    create_urls = partial(generate_urls, out_dir=output)
    urls = list(map(create_urls, records))
    download = partial(fetch_data, tmp=tmp)
    pool = Pool(cpu_count() - 1)
    print("Download marine sequence data from FTP server")
    pool.map(download, urls)
    pool.close()
    pool.join()
    move_atomic(tmp, output)


if __name__ == "__main__":
    input, out, email, key = main(sys.argv[1:])
    Entrez.email = email
    Entrez.api = key
    root = "/tmp"
    # Remove tmp directory if exists
    remove_tmp(root + "/tmp")
    tmp = create_subdir(root + "/tmp")
    run(input, out, tmp)
    remove_tmp(root + "/tmp")
