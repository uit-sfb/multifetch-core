import xml.etree.ElementTree as ET
from utils import *
import urllib3
from http.client import responses
import json


def getBioSampleXMLData(data):
    root = ET.fromstring(data)
    attrs = root.findall('./SAMPLE/SAMPLE_ATTRIBUTES/SAMPLE_ATTRIBUTE/TAG')
    attr_vals = root.findall('./SAMPLE/SAMPLE_ATTRIBUTES/SAMPLE_ATTRIBUTE/VALUE')
    data = dict([elem for elem in zip(attrs, attr_vals)])
    tagsAndValues = dict([(t.text, v.text) for t, v in data.items()])
    lat_lon = getLatLonXML(tagsAndValues)
    other_info = [ ('biosample:'+elem[0], elem[1]) for elem in list(tagsAndValues.items()) if elem[0] not in LATLON_LIST ]
    resList = other_info + [lat_lon]
    return resList


def processBioSampleDataXML(accession, id):
    baseUrl = "https://www.ebi.ac.uk/ena/browser/api/xml"
    url = baseUrl + '/' + str(id)
    http = urllib3.PoolManager()
    request = http.request('GET', url)
    http_status = request.status
    http_desc = responses[http_status]
    if not (http_desc == "OK"):
        # print("{}.xml not fetched, reason: ".format(id) + str(http_status) + " " + str(http_desc))
        return []
    data = request.data.decode('UTF-8')
    res = getBioSampleXMLData(data)
    resList = [('acc:genbank', accession)] + res
    return resList


def processBioSampleData(accession, id):
    baseUrl = "https://www.ebi.ac.uk/biosamples/samples"
    url = baseUrl + '/' + str(id) + '.json'
    http = urllib3.PoolManager()
    request = http.request('GET', url)
    http_status = request.status
    http_desc = responses[http_status]
    df = pd.DataFrame()
    if not (http_desc == "OK"):
        res = processBioSampleDataXML(accession, id)
    else:
        data = json.loads(request.data.decode('UTF-8'))
        chars = data['characteristics']
        lat_lon = geLatLonData(chars)
        other_info = [processAttributes(k, v) for k, v in chars.items() if str(k) not in LATLON_LIST ]
        res = [('acc:genbank', accession), lat_lon] + [v for v in other_info if v]
    if res:
        df = pd.DataFrame(dict(res), index=[0])
    if not df.empty:
        df.columns = list(map(formatColumn, list(df.columns.values)))
    return df
