FROM python:3.9.5

LABEL maintainer="mmp@uit.no" \
  toolName=multifetch \
  url=https://gitlab.com/uit-sfb/multifetch

WORKDIR /app

ENV HOME /app

RUN chmod a+rwx $HOME

COPY /src/requirements.txt /src/*.py ./

RUN pip install --no-cache-dir -r requirements.txt
